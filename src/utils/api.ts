import axios from 'axios';
import * as ls from 'local-storage';

import { API_URL, LS_KEYS } from './constants';

const getApi = (extra: string) => {
    return `${API_URL}${extra}`;
};

export async function login(email: string, password: string) {
    const { data } = await axios.post(getApi('/auth/login'), {
        email,
        password,
    });

    ls.set(LS_KEYS.LOGIN_DATA, data);
    return data;
}

export async function signup(email: string, password: string) {
    console.log(getApi('/auth/sign-up'));
    await axios.post(getApi('/auth/sign-up'), {
        name: email,
        email,
        password,
    });
    const data = await login(email, password);

    ls.set(LS_KEYS.LOGIN_DATA, data);
    return data;
}
