import * as ls from 'local-storage';

import { IForm } from '../components/FormView/interfaces';

export const IS_DEV = process.env.NODE_ENV === 'development';

export const LS_KEYS = {
    DEFAULT_FORM: 'DEFAULT_FORM',
    LOGIN_DATA: 'LOGIN_DATA',
};

// export const API_URL = process.env.API_URL ?? `http://${window.location.hostname}:8080`;
export const API_URL = 'https://sberapi.gainanov.pro';
console.log(`API_URL=${process.env.API_URL}; NODE_ENV=${process.env.NODE_ENV}`);

export const defaultForm: IForm = ls.get(LS_KEYS.DEFAULT_FORM) || {
    logoUrl: 'https://apply.sbercloud.org/assets/images/side.png',
    title: 'С Возвращением!',
    description: 'Мы так рады видеть вас снова!',
    submit: {
        text: 'Отправить',
    },
    components: [],
    styles: {
        background: '#36393f',
    },
    networks: {
        fb: false,
        vk: false,
        google: false,
    },
};
