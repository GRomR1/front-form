export const wait = (ms: number = 1500) => new Promise((res) => setTimeout(res, ms));
