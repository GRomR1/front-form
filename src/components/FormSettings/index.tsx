import * as ls from 'local-storage';
import {
    Button,
    TextField,
    FormControl,
    FormGroup,
    FormControlLabel,
    Switch,
    Accordion,
    AccordionSummary,
    AccordionDetails,
    Typography,
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import styled from 'styled-components';
import { ChromePicker } from 'react-color';

import { FormView } from '../FormView';
import { defaultForm, LS_KEYS } from '../../utils/constants';
import { useState } from 'react';
import { IForm } from '../FormView/interfaces';
import { FormIntegration } from '../FormIntegration';

const FormSettingsWrapper = styled.div`
    display: flex;

    .col {
        flex: 1;
    }

    .form-settings {
        text-align: left;
        padding: 10px 20px;
    }
`;

const FormSettingsForm = styled.form`
    .form-settings__form-inputs {
        display: flex;
        flex-direction: column;
        & > div {
            margin-bottom: 20px;
        }
    }

    .form-settings__form-bottom-bar {
        padding: 20px 0;

        button {
            margin-right: 20px;
        }
    }

    .form-settings__form-networks {
        margin: 20px 0;
    }
`;

export function FormSettings() {
    const [view, changeView] = useState({
        showBgPicker: false,
        fb: true,
        vk: false,
        google: true,
    });
    const [formData, changeFormData] = useState<IForm>(defaultForm);
    const { networks } = formData;

    return (
        <>
            <FormSettingsWrapper>
                <div className="col form-settings">
                    <h2>Настройки формы </h2>
                    <FormSettingsForm autoComplete="off">
                        <div className="form-settings__form-inputs">
                            <TextField
                                label="Ссылка на логотип"
                                value={formData.logoUrl}
                                onChange={(e) => {
                                    const value = e.target.value;

                                    changeFormData({
                                        ...formData,
                                        logoUrl: value,
                                    });
                                }}
                            />
                            <TextField
                                label="Заголовок"
                                value={formData.title}
                                onChange={(e) => {
                                    const value = e.target.value;

                                    changeFormData({
                                        ...formData,
                                        title: value,
                                    });
                                }}
                            />
                            <TextField
                                label="Описание"
                                value={formData.description}
                                onChange={(e) => {
                                    const value = e.target.value;

                                    changeFormData({
                                        ...formData,
                                        description: value,
                                    });
                                }}
                            />
                            <TextField
                                label="Кнопка отправки"
                                value={formData.submit.text}
                                onChange={(e) => {
                                    const value = e.target.value;

                                    changeFormData({
                                        ...formData,
                                        submit: {
                                            ...formData.submit,
                                            text: value,
                                        },
                                    });
                                }}
                            />
                        </div>

                        <Button
                            variant="outlined"
                            color="primary"
                            onClick={() => changeView({ ...view, showBgPicker: !view.showBgPicker })}
                        >
                            Цвет фона
                        </Button>
                        {view.showBgPicker && (
                            <ChromePicker
                                color={formData.styles.background}
                                onChange={(color) =>
                                    changeFormData({
                                        ...formData,
                                        styles: { ...formData.styles, background: color.hex },
                                    })
                                }
                            />
                        )}

                        <div className="form-settings__form-networks">
                            <FormControl component="fieldset">
                                <p>Социальные сети</p>
                                <FormGroup>
                                    <FormControlLabel
                                        control={
                                            <Switch
                                                checked={networks.vk}
                                                onChange={(e) =>
                                                    changeFormData({
                                                        ...formData,
                                                        networks: {
                                                            ...formData.networks,
                                                            vk: e.target.checked,
                                                        },
                                                    })
                                                }
                                                name="vk"
                                                color="primary"
                                            />
                                        }
                                        label="Вконтакте"
                                    />
                                    <FormControlLabel
                                        control={
                                            <Switch
                                                checked={networks.google}
                                                onChange={(e) =>
                                                    changeFormData({
                                                        ...formData,
                                                        networks: {
                                                            ...formData.networks,
                                                            google: e.target.checked,
                                                        },
                                                    })
                                                }
                                                name="google"
                                                color="primary"
                                            />
                                        }
                                        label="Фейсбук"
                                    />
                                    <FormControlLabel
                                        control={
                                            <Switch
                                                checked={networks.fb}
                                                onChange={(e) =>
                                                    changeFormData({
                                                        ...formData,
                                                        networks: {
                                                            ...formData.networks,
                                                            fb: e.target.checked,
                                                        },
                                                    })
                                                }
                                                name="fb"
                                                color="primary"
                                            />
                                        }
                                        label="гугл"
                                    />
                                </FormGroup>
                            </FormControl>
                        </div>

                        <div className="form-settings__form-bottom-bar">
                            <Button variant="outlined">В панель управления</Button>
                            <Button variant="contained" onClick={() => changeFormData(defaultForm)}>
                                Сбросить
                            </Button>
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={() => {
                                    ls.set(LS_KEYS.DEFAULT_FORM, formData);
                                }}
                            >
                                Сохранить
                            </Button>
                        </div>
                    </FormSettingsForm>

                    <Accordion>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel1a-content"
                            id="panel1a-header"
                        >
                            <Typography>Код виджета</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <FormIntegration />
                        </AccordionDetails>
                    </Accordion>
                </div>

                <div className="col">
                    <FormView data={formData} />
                </div>
            </FormSettingsWrapper>

            {/* <Rnd
                style={style}
                // disableDragging
                default={{
                    x: 0,
                    y: 0,
                    width: 320,
                    height: 200,
                }}
            >
                Rnd
            </Rnd> */}
        </>
    );
}
