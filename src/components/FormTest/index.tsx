import * as ls from 'local-storage';
import styled from 'styled-components';
import { useHistory } from 'react-router-dom';
import { Button } from '@material-ui/core';
import { EmojiEmotions as SmileIcon } from '@material-ui/icons';

import { SyntaxPreviewer } from '../SyntaxPreviewer';
import { Container } from '../layouts/Container';
import { LS_KEYS } from '../../utils/constants';

const Header = styled.header`
    background-color: #2b2b33;
    color: #fff;
    padding: 50px 0;
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    font-size: 20px;
`;

export function FormTest() {
    const history = useHistory();
    const codeString = `${JSON.stringify(ls.get(LS_KEYS.LOGIN_DATA))}`;

    return (
        <>
            <Header>
                <SmileIcon fontSize="large" />
                <p>Все работает!</p>
            </Header>

            <Container>
                <p>
                    Если вы видите эту страницу, то соединение работает. Это данные пользователя, которые были получены:
                </p>

                <SyntaxPreviewer code={codeString} />

                <Button
                    type="button"
                    onClick={() => {
                        history.push('/form-settings');
                    }}
                >
                    Вернуться к настройкам
                </Button>
            </Container>
        </>
    );
}
