import styled from 'styled-components';

import { API_URL } from '../../utils/constants';
import { SyntaxPreviewer } from '../SyntaxPreviewer';

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    max-width: 600px;
`;

export function FormIntegration() {
    const codeString = `
<!-- Guardify Widget BEGIN -->
<div class="guardify-container">
  <div class="guardify-container__widget"></div>
  <script type="text/javascript" src="${API_URL}/external-embedding/embed-widget-guardify.js" async>
  {
    "width": 425,
    "height": 450,
    "colorTheme": "light"
  }
  </script>
</div>
<!-- Guardify Widget END -->   
`;
    return (
        <Wrapper>
            <p>Вставьте код ниже для того чтобы интегрировать веб-форму:</p>

            <SyntaxPreviewer code={codeString} />
        </Wrapper>
    );
}
