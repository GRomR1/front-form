import { Button, TextField } from '@material-ui/core';
import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';

import { signup } from '../../utils/api';
import { defaultForm } from '../../utils/constants';
import { wait } from '../../utils/tools';
import { IForm, IFormStyles } from './interfaces';

const FormTitle = styled.h1`
    font-size: 1.5em;
    text-align: center;
    color: #444;
`;

const FormWrapper = styled.div<IFormStyles>`
    display: flex;
    align-items: center;
    justify-content: center;
    background: ${(props) => props.background || ' #36393f'};
    height: 100vh;
`;

const Form = styled.form`
    background-color: #fff;
    border-radius: 4px;
    flex: 1;
    max-width: 400px;
    padding: 20px 10px;
    display: flex;
    flex-direction: column;
    border: 1px solid rgb(0 0 0 / 50%);
    box-shadow: 0 0 4px rgb(0 0 0 / 50%);

    .form-view {
        &__input {
            margin-top: 10px;
            margin-bottom: 20px;
        }

        &__logo {
            width: 100%;
            height: auto;
        }
    }
`;

interface IFormViewProps {
    data?: IForm;
}

export function FormView(props: IFormViewProps) {
    const [loginData, changeLoginData] = useState({
        login: '',
        password: '',
    });
    const history = useHistory();
    const { data } = props;
    const { title, description, submit, logoUrl, styles } = data || defaultForm;

    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        await wait(1000);
        await signup(loginData.login, loginData.password);
        history.push('/form-test');
    };

    return (
        <FormWrapper background={styles.background}>
            <Form autoComplete="off" onSubmit={handleSubmit}>
                {logoUrl && <img className="form-view__logo" src={logoUrl} alt="logo" />}
                {title && <FormTitle>{title}</FormTitle>}
                {description && <p>{description}</p>}
                <TextField
                    className="form-view__input"
                    label="Логин"
                    variant="outlined"
                    value={loginData.login}
                    onChange={(e) => changeLoginData({ ...loginData, login: e.target.value })}
                />
                <TextField
                    className="form-view__input"
                    label="Пароль"
                    type="password"
                    variant="outlined"
                    value={loginData.password}
                    onChange={(e) => changeLoginData({ ...loginData, password: e.target.value })}
                />

                <Button type="submit" variant="contained" color="primary" size="large">
                    {submit.text}
                </Button>

                {/* <p>
                    Нужна учётная запись? <a href="/">Зарегистрироваться</a>
                </p> */}
            </Form>
        </FormWrapper>
    );
}
