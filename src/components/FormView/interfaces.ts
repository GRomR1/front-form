export interface IFormComponent {}

export interface IFormSubmit {
    text: string;
}

export interface IFormStyles {
    // https://github.com/lindelof/particles-bg
    background?: string;
}

export interface IForm {
    logoUrl?: string;
    title?: string;
    description?: string;
    submit: IFormSubmit;
    components: IFormComponent[];
    styles: IFormStyles;
    networks: {
        fb: boolean;
        vk: boolean;
        google: boolean;
    };
}
