import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import styled from 'styled-components';

import { ROUTES } from './routes';
import './App.css';
import { IS_DEV } from './utils/constants';

const Menu = styled.ul`
    display: flex;
    flex-direction: row-reverse;
    border: orange 1px solid;
    margin: 0;
    padding: 10px 20px;

    li {
        display: inline-block;
        padding: 4px 10px;
    }
`;

export default function App() {
    const style = {};

    return (
        <div style={style} className="App">
            <Router>
                <div>
                    {IS_DEV && (
                        <Menu>
                            {ROUTES.map((it) => (
                                <li key={it.title}>
                                    <Link to={it.to}>{it.title}</Link>
                                </li>
                            ))}
                        </Menu>
                    )}

                    <Switch>
                        {ROUTES.map((it) => (
                            <Route key={it.title} path={it.to}>
                                {it.component}
                            </Route>
                        ))}
                    </Switch>
                </div>
            </Router>
        </div>
    );
}
