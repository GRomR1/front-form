import { FormSettings } from './components/FormSettings';
import { FormView } from './components/FormView';
import { FormTest } from './components/FormTest';

export const ROUTES = [
    {
        to: '/form-settings',
        component: (props: any) => <FormSettings {...props} />,
        title: 'Settings',
    },
    {
        to: '/form-view',
        component: (props: any) => <FormView {...props} />,
        title: 'View',
    },
    {
        to: '/form-test',
        component: (props: any) => <FormTest {...props} />,
        title: 'Form test',
    },
];
