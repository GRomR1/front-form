FROM    node:latest
ARG     EXPOSE_PORT=3000

ARG     API_URL=http://127.0.0.1:8080
ENV     API_URL=${API_URL}

ARG     NODE_ENV=production
ENV     NODE_ENV=${NODE_ENV}

ENV PATH /app/node_modules/.bin:$PATH

# install app dependencies
COPY package.json ./
COPY package-lock.json ./
RUN npm install --silent

# add app
COPY . ./

# start app

# our last part
WORKDIR     /app
EXPOSE      ${EXPOSE_PORT}
ENTRYPOINT  []
CMD         ["npm", "start"]
