FROM    nginx:latest

ARG     EXPOSE_PORT=80

COPY    nginx.conf /etc/nginx/nginx.conf

COPY    ./build /app

# our last part
WORKDIR     /app
EXPOSE      ${EXPOSE_PORT}
ENTRYPOINT  []
CMD         ["nginx", "-g", "daemon off;"]
