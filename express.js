/**
 * Экспресс-сервер для обработки статики Реакта
 * Создан для тех случаев, когда докер не получится запустить на хосте
 */
const express = require('express');
const path = require('path');
const cors = require('cors');

const CLIENT_APP_PATH = './build';

const app = express();
app.use(express.static(CLIENT_APP_PATH));
app.use(cors());
app.get('/*', (req, res) => {
    res.sendFile(path.join(__dirname, CLIENT_APP_PATH, 'index.html'), (err) => {
        if (err) {
            res.status(500).send(err);
        }
    });
});

const PORT = 80;

app.listen(PORT, () => console.log(`app listening at http://localhost:${PORT}`));
